/*
 * Builder.cpp
 *
 *       
 */

#define DEBUG_TYPE "lerna_builder"
#include "llvm/Support/Debug.h"
#include "llvm/Transforms/IPO.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Attributes.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/Pass.h"
#include "llvm/Transforms/Utils/Local.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/MemoryDependenceAnalysis.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Lerna/Lerna.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Transforms/Utils/ValueMapper.h"
#include <sstream>
#include <string>
using namespace llvm;
using namespace lerna;
//#define USE_TANGER
#define MAX_PARA_TASKS 100

#define DEFAULT_EXIT 1000

#define _32_BIT

STATISTIC(NumExtracted, "Number of loops extracted");

static cl::opt<std::string>
IncludeLoop("include-loop",
					cl::init("-"),
					cl::value_desc("<header basic block>@<function name>"),
                    cl::desc("Only parallelize given loop"));

static cl::opt<std::string>
ExcludeLoop("exclude-loop",
					cl::init("0"),
                    cl::value_desc("<header basic block>@<function name>"),
                    cl::desc("Parallelize all except given loop"));

static cl::opt<int>
LoopIDOffset("loop-offset",
					cl::init(0),
                    cl::value_desc("<number>"),
                    cl::desc("Multiplier for loop-id generation"));

namespace lerna{
typedef SetVector<Value*> Values;

class LernaBuilder: public LernaLoopPass {
public:
	static char ID;
	unsigned NumLoops;
	static int loop_id_generator;

	SetVector<BasicBlock*> BlocksToExtract;
	DominatorTree* DT;

	unsigned NumExitBlocks;

	PHINode *IndVar;
	PHINode *newIndVar;

#ifdef USE_TANGER
	Constant *FuncTangerBegin;
	Constant *FuncTangerCommit;
#endif
	Constant *FuncLernaAddJob;
	Constant *FuncLernaEndBatch;
	Constant *FuncLernaCount;
	Constant *FuncLernaNewBatch;

	explicit LernaBuilder(unsigned numLoops = ~0) :
		LernaLoopPass(ID), NumLoops(numLoops) {
	}

	virtual bool runOnLoop(Loop *L, LPPassManager &LPM);
	void cleanGlobals();

	virtual void getAnalysisUsage(AnalysisUsage &AU) const {
		AU.addRequiredID(BreakCriticalEdgesID);
		AU.addRequiredID(LoopSimplifyID);
		AU.addRequired<DominatorTreeWrapperPass> ();
	}

private:

	void initLernaCallBacks(Module* module);
	void initTangerCallBacks(Module* module);

	void inlineCalls(SmallVector<BasicBlock*, 8> blocks);

	bool definedInRegion(Value *V) const {
		if (Instruction *I = dyn_cast<Instruction>(V))
			if (BlocksToExtract.count(I->getParent()))
				return true;
		return false;
	}

	/*
	 * definedInCaller - Return true if the specified value is defined in the
	 * function being code extracted, but not in the region being extracted.
	 * These values must be passed in as live-ins to the function.
	 */
	bool definedInCaller(Value *V) const {
		if (isa<Argument> (V))
			return true;
		if (Instruction *I = dyn_cast<Instruction>(V))
			if (!BlocksToExtract.count(I->getParent()))
				return true;
		return false;
	}

//	/*
//	 *  FindPhiPredForUseInBlock - Given a value and a basic block, find a PHI
//	 *  that uses the value within the basic block, and return the predecessor
//	 *  block associated with that use, or return 0 if none is found.
//	 */
//	BasicBlock* findPhiPredForUseInBlock(Value* Used, BasicBlock* BB) {
//		for (Value::user_iterator UI = Used->user_begin(), UE = Used->user_end(); UI
//				!= UE; ++UI) {
//			PHINode *P = dyn_cast<PHINode> (*UI);
//			if (P && P->getParent() == BB)
//				return P->getIncomingBlock(*UI);
//		}
//
//		return 0;
//	}

	Function *ExtractCodeRegion(
			BasicBlock* preHeader,
			BasicBlock* header,
			BasicBlock* latch,
			SmallVector<BasicBlock*, 8> code);

	Function *constructFunction(
			LLVMContext& context,
			Module *module,
			BasicBlock* header,
			const Values &inputs,
			Twine name,
			bool throwing,
			bool definitionOnly);
};
}

int LernaBuilder::loop_id_generator = 0;

char LernaBuilder::ID = 0;

static RegisterPass<LernaBuilder>
Y("builder", "LernaVM: Reconstruct loops to run iterations in parallel as functions");

char &lerna::LernaBuilderID = LernaBuilder::ID;
LoopPass *createLernaBuilderPass() {
	return new LernaBuilder();
}

#ifdef USE_TANGER
void LernaBuilder::initTangerCallBacks(Module* module) {
	LLVMContext &Context = module->getContext();
	std::vector<const Type *> TyFuncLernaInitArgs;
	FunctionType *TyFuncTanger = FunctionType::get(Type::getVoidTy(Context),
			TyFuncLernaInitArgs, false);
	// -------------      void tanger_begin()                                     ------------//
	FuncTangerBegin = module->getOrInsertFunction("tanger_begin", TyFuncTanger);
	// -------------      void tanger_commit()                                    ------------//
	FuncTangerCommit = module->getOrInsertFunction("tanger_commit",	TyFuncTanger);
}
#endif

// Define Lerna Runtime call-backs
void LernaBuilder::initLernaCallBacks(Module* module) {
	LLVMContext &Context = module->getContext();
	// ------------- int  lernaruntime_addJob(void* args, int (*work_para)(void*), int (*work_seq)(void*), int nested) ------------//
	Type* arr0[] = {
			(Type *) PointerType::getUnqual(IntegerType::getInt8Ty(Context)),
	};
    ArrayRef<Type *> TyFuncGeneratedArgs(arr0);
	Type* arr1[] = {
			(Type *) PointerType::getUnqual(IntegerType::getInt8Ty(Context)),
			(Type *) PointerType::getUnqual(FunctionType::get(Type::getVoidTy(Context), TyFuncGeneratedArgs, false)),
			(Type *) PointerType::getUnqual(FunctionType::get(Type::getVoidTy(Context), TyFuncGeneratedArgs, false)),
			IntegerType::getInt8Ty(Context)
	};
    ArrayRef<Type *> TyFuncLernaDispatchArgs(arr1);
    FunctionType *TyFuncLernaDispatch = FunctionType::get(IntegerType::getInt8Ty(Context), TyFuncLernaDispatchArgs, false);
    FuncLernaAddJob = module->getOrInsertFunction(HYDRARUNTIME_ADD_JOB, TyFuncLernaDispatch);
	// ------------- int lernaruntime_endBatch(int nested) ------------//
    Type* arr2[] = {
    		IntegerType::getInt8Ty(Context),
    };
    ArrayRef<Type *> TyFuncLernaSyncArgs(arr2);
	FunctionType *TyFuncLernaSync = FunctionType::get(Type::getInt32Ty(Context), TyFuncLernaSyncArgs, false);
	FuncLernaEndBatch = module->getOrInsertFunction(HYDRARUNTIME_END_BATCH, TyFuncLernaSync);
	// ------------- int lernaruntime_getJobsCount() ------------//
	FunctionType *TyFuncLernaCount = FunctionType::get(Type::getInt32Ty(Context), false);
	FuncLernaCount = module->getOrInsertFunction(HYDRARUNTIME_JOBS_COUNT, TyFuncLernaCount);
	// ------------- void lernaruntime_init() ------------//
	Type* arr3[] = {
			IntegerType::getInt32Ty(Context),
	};
	ArrayRef<Type *> TyFuncLernaInitArgs(arr3);
	FunctionType *TyFuncLernaInit = FunctionType::get(IntegerType::getInt8Ty(Context), TyFuncLernaInitArgs, false);
	FuncLernaNewBatch = module->getOrInsertFunction(HYDRARUNTIME_NEW_BATCH, TyFuncLernaInit);
}

void LernaBuilder::cleanGlobals() {

	BlocksToExtract.clear();

	DT = &getAnalysis<DominatorTreeWrapperPass> ().getDomTree();

	NumExitBlocks = 0;

	IndVar = NULL;
	newIndVar = NULL;

#ifdef USE_TANGER
	FuncTangerBegin = NULL;
	FuncTangerCommit = NULL;
#endif

	FuncLernaAddJob = NULL;
	FuncLernaEndBatch = NULL;
	FuncLernaCount = NULL;
	FuncLernaNewBatch = NULL;

}

void LernaBuilder::inlineCalls(SmallVector<BasicBlock*, 8> blocks) {
	for(int i=0; i<blocks.size(); i++){
		BasicBlock* bb = blocks[i];
		for(BasicBlock::iterator j=bb->begin();j!=bb->end();++j){
			Function *F = NULL;
			if(CallInst *CI = dyn_cast<CallInst>(j)){
				F = CI->getCalledFunction();
			}else if(InvokeInst *II = dyn_cast<InvokeInst>(j)){
				F = II->getCalledFunction();
			}
			if (F){
			  bool inlined = F->getAttributes().hasAttrSomewhere(Attribute::AlwaysInline);
			  if(inlined){
				  dbgs() << "\t\t\tAlreadey Inline " + F->getName() + "\n";
			  }else if(lookupDictionary(F, "SAFE.txt")){
				  dbgs() << "\t\t\tSkip Safe " + F->getName() + "\n";
			  }else if(lookupDictionary(F, "DICTIONARY.txt")){
				  dbgs() << "\t\t\tForce Inline " + F->getName() + "\n";
				  F->addAttribute(~0, Attribute::AlwaysInline);
				  Function::BasicBlockListType &code = F->getBasicBlockList();
				  SmallVector<BasicBlock*, 8> f_blocks;
				  for (Function::BasicBlockListType::iterator bs = code.begin(), es = code.end(); bs != es; ++bs) {
					  f_blocks.push_back(&(*bs));
				  }
				  inlineCalls(f_blocks);
			  }else{
				  dbgs() << "\t\t\tExternal " + F->getName() + "\n";
			  }
		   }
		}
	}
}

bool LernaBuilder::runOnLoop(Loop *L, LPPassManager &LPM) {
	cleanGlobals();

	LOG(dbgs() << "Builder: Processing Loop '" << L->getBlocks().front()->getName() << "' at " << L->getBlocks().front()->getParent()->getName() << "() : Blocks = " << L->getBlocks().size() << "\n");
	IndVar = L->getCanonicalInductionVariable();

	// Nesting is not supported, so skip if I can extract & parallelize parent loop.
	Loop *parent = L;
	while ((parent = parent->getParentLoop())) {
		if (checkExtractableLoop(parent) == EXTRACTABLE) {
			// can extract parent
			LOG(dbgs() << "\tSkip: Inner Loop, found eligible parent '" << parent->getBlocks().front()->getName() << "'\n");
			return false;
		}
	}

	if (checkExtractableLoop(L) != EXTRACTABLE) {
		LOG(dbgs() << "\tSkip: Not Extractable\n");
		return false;
	}
	if (NumLoops == 0) {
		LOG(dbgs() << "\tSkip: Zero Loop\n");
		return false;
	}

	std::string ss = std::string(L->getHeader()->getName()) + "@" + std::string(L->getHeader()->getParent()->getName());

	if(IncludeLoop != "-"){
		std::stringstream sss(IncludeLoop);
		std::string item;
		bool foundInArg = false;
		while (!foundInArg && std::getline(sss, item, ',')) {
			if(ss == item){
				foundInArg = true;
			}
		}
		if(!foundInArg){
			LOG(dbgs() << "Skip Loop '" << ss << "' [ include-loop='" << IncludeLoop << "' ]\n");
			return false;
		}
	}

	if(ExcludeLoop != "-"){
		std::stringstream sss(ExcludeLoop);
		std::string item;
		while (std::getline(sss, item, ',')) {
			if(ss == item){
				LOG(dbgs() << "Skip Loop '" << ss << "' [ exclude-loop='" << ExcludeLoop << "' ]\n");
				return false;
			}
		}
	}

	LOG(dbgs() << "\t=====================================\n");
	LOG(dbgs() << "\t       ELIGIBLE:  '" << ss << "' loop-id=" << (loop_id_generator + LoopIDOffset) << "\n");
	LOG(dbgs() << "\t=====================================\n");

	--NumLoops;

	DBG(dbgs() << "\tInit callbacks\n");
	Module* module = L->getHeader()->getParent()->getParent();
	initLernaCallBacks(module);

#ifdef USE_TANGER
	initTangerCallBacks(module);
#endif
//	DBG(dbgs() << "Induction: " << *L->getCanonicalInductionVariable() << "\n");
	SmallVector<BasicBlock*, 8> exitBlocks;
	L->getExitBlocks(exitBlocks);
	DBG(dbgs() << "\tExits = " << exitBlocks.size() << "\n");

	SmallVector<BasicBlock*, 8> blocksToExtract;
	for (unsigned i = 0; i != L->getBlocks().size(); ++i) {
		BasicBlock* bb = L->getBlocks()[i];
		blocksToExtract.push_back(bb);
		for(BasicBlock::iterator j=bb->begin();j!=bb->end();++j){
			if(InvokeInst *II = dyn_cast<InvokeInst>(j)){
				DBG(dbgs() << "\tInvoke Instruction : " << *II  << "\n");
				if(II->getUnwindDest()!=NULL){
					blocksToExtract.push_back(II->getUnwindDest());
					DBG(dbgs() << "\t\tUnwind:" << II->getUnwindDest()->getName().str() << "\n");
				}
			}
		}
	}
	inlineCalls(blocksToExtract);

	DBG(dbgs() << "\tExtract Region = " << blocksToExtract.size() << "\n");
	ExtractCodeRegion(L->getLoopPreheader(), L->getHeader(), L->getLoopLatch(), blocksToExtract);

	// After extraction, the loop is replaced by a function call, so we shouldn't try to run any more loop passes on it.
	Loop* itr = L->getParentLoop();
	while(itr){
		DBG(dbgs() << "Delete Loop:" << itr->getHeader()->getName() << " size=" << itr->getBlocks().size() << "\n");
		DBG(dbgs() << "{\n");
		for (unsigned i = 0; i != itr->getBlocks().size(); ++i) {
			BasicBlock* bb = itr->getBlocks()[i];
			DBG(dbgs() <<  "\t" << i << "-" << bb->getName() << "\n");
		}
		DBG(dbgs() << "}\n");
		Loop* p = itr->getParentLoop();
		LPM.deleteLoopFromQueue(itr);
		itr = p;
	}
	LPM.deleteLoopFromQueue(L);	// We must delete loop after their parents, not before! Otherwise parents won't be deleted!!!
	++NumExtracted;
	LOG(dbgs() << "\tDone :)\n");
	return true;
}

Function *LernaBuilder::ExtractCodeRegion(
		BasicBlock* preHeader,
		BasicBlock* header,
		BasicBlock* latch,
		SmallVector<BasicBlock*, 8> code) {
	/*__________________________________________________________________________________
	 *
	 * 		(1) Initialization
	 * __________________________________________________________________________________
	 */
	Function *oldFunction = preHeader->getParent();
	Module* module = oldFunction->getParent();
	LLVMContext& context = header->getContext();
	BlocksToExtract.insert(code.begin(), code.end());

	DBG(dumb(oldFunction, "(1) Original: ")); // print the original function before any transformation
	/*__________________________________________________________________________________
	 *
	 * 		(2) Add boundary blocks (header, dispatcher & syncExit)
	 * __________________________________________________________________________________
	 */
	DBG(dbgs() << "Add boundaries blocks\n");
	Value* arr1[] = {
		ConstantInt::getIntegerValue(IntegerType::getInt32Ty(context), APInt(32, loop_id_generator++ + LoopIDOffset))
	};
	ArrayRef<Value*> ParamFuncLernaInit(arr1);
	CallInst *callInit = CallInst::Create(FuncLernaNewBatch, ParamFuncLernaInit, "", preHeader->getTerminator());

	BasicBlock *syncExit = BasicBlock::Create(context,	"syncExit", oldFunction, header);

	BasicBlock *dispatcher = BasicBlock::Create(context,	"dispatcher", oldFunction, header);

	BasicBlock *newBatch = BasicBlock::Create(context,	"newBatch", oldFunction, header);
	CallInst::Create(FuncLernaNewBatch, ParamFuncLernaInit, "", newBatch);
	newBatch->getInstList().push_back(BranchInst::Create(dispatcher));
	latch->getTerminator()->replaceUsesOfWith(header, newBatch);

	BasicBlock *newHeader = BasicBlock::Create(context,	"header", oldFunction, header);
	newHeader->getInstList().push_back(BranchInst::Create(dispatcher));
	preHeader->getTerminator()->replaceUsesOfWith(header, newHeader);

	DBG(dumb(oldFunction, "(2) With Boundaries: "));
	/*__________________________________________________________________________________
	 *
	 * 		(3) Replace Induction Variable
	 * _________________________________________________________________________________
	 */
	DBG(dbgs() << "Replace Induction variable\n");
	// get the actual number of executed jobs
	CallInst *callJobsCount = CallInst::Create(FuncLernaCount);
	syncExit->getInstList().push_front(callJobsCount);

	Instruction::CastOps OpCastStruct = CastInst::getCastOpcode(callJobsCount, false, IndVar->getType(), false);
	// Cast the struct to void*
	CastInst* castCountInst = CastInst::Create(OpCastStruct, callJobsCount, IndVar->getType(), "castCount", syncExit);

	// create new induction variable incremented by 1
	newIndVar = PHINode::Create(IndVar->getType(), 2, "newIndVar", &newHeader->getInstList().front());
	BinaryOperator *addIndVar = BinaryOperator::CreateNSWAdd(newIndVar, ConstantInt::get(newIndVar->getType(), 1), "", dispatcher);
	newIndVar->addIncoming(ConstantInt::get(newIndVar->getType(), 0), preHeader);
	newIndVar->addIncoming(addIndVar, dispatcher);
	// replace the old IndVar with either
	//	1) New IndVar (Extracted blocks) or
	//  2) Actual number of executed jobs (Not extracted blocks)
	std::vector<User*> IndVarUsers(IndVar->user_begin(), IndVar->user_end());	// have to take it aside into a vector, as we change it inside the loop
	for (std::vector<User*>::iterator use = IndVarUsers.begin(), useE = IndVarUsers.end(); use != useE; ++use){
		if (Instruction* I = dyn_cast<Instruction>(*use)){
			if (BlocksToExtract.count(I->getParent())){
				DBG(dbgs() << "\tExtracted uses IndVar: " << *I << "\n");
				I->replaceUsesOfWith(IndVar, newIndVar);
			}
			else{
				DBG(dbgs() << "\tExternal uses IndVar: " << *I << "\n");
				I->replaceUsesOfWith(IndVar, castCountInst);
			}
		}else{
			DBG(dbgs() << "\tNot an Instruction uses IndVar: " << *use << "\n");
		}
	}
	// Demote all PHIs at the old loop header (including the old IndVar)
	while(header->getFirstNonPHI() != &header->getInstList().front())
		if(PHINode *PN = dyn_cast<PHINode> (header->getFirstNonPHI()->getPrevNode()))
			DemotePHIToStack(PN, 0);

	DBG(dumb(oldFunction, "(3) IndVar Replaced: "));

	/*__________________________________________________________________________________
	 *
	 * 		(4) Detect Inputs and eliminate Outputs (demote to stack)
	 * _________________________________________________________________________________
	 */
	DBG(dbgs() << "Detect I/O \n");
	Values inputs;
//	SetVector<Value**> external_outputs;
	bool checkAgain = false;
	do {
		if (checkAgain) {
			// initialize
			inputs.clear();
			checkAgain = false;
			DBG(dumb(header->getParent(), "I/O"));
		}
		Values outputs;

		// detect inputs/outputs of the extracted area
		for (SetVector<BasicBlock*>::const_iterator ci = BlocksToExtract.begin(), ce = BlocksToExtract.end(); ci != ce; ++ci) {
			BasicBlock *BB = *ci;
			DBG(dbgs() << "I/O Check [" << BB->getName() << "]\n");

			for (BasicBlock::iterator I = BB->begin(), E = BB->end(); I	!= E; ++I) {
				DBG(dbgs() << "\t U:" << *I << "\n");
				// If a used value is defined outside the region, it's an input.  If an instruction is used outside the region, it's an output.
				for (User::op_iterator O = I->op_begin(), E = I->op_end(); O != E; ++O)
					if (definedInCaller(*O)) {
						DBG(dbgs() << "\t\t Input: " << (*O)->getName().data() << "\n");
						inputs.insert(*O);
					}

				// Consider uses of this instruction (outputs).
				for (Value::user_iterator UI = I->user_begin(), E = I->user_end(); UI != E; ++UI)
					if (!definedInRegion(*UI)) {
						DBG(dbgs() << "\t\t Output: " << *I << "\n");
						outputs.insert(I);
						break;
					}
			}
		}

		DBG(dbgs() << "Inputs: " << inputs.size() << "\n");
		DBG(dbgs() << "Outputs: " << outputs.size() << "\n");

		// eliminate any ouput by demote it to stake
		for (unsigned i = 0, e = outputs.size(); i != e; ++i) {
			DBG(dbgs() << "\tOutput:" << *outputs[i] << "\n");
			for (Value::user_iterator UI = outputs[i]->user_begin(), E = outputs[i]->user_end(); UI != E; ++UI) {
				if (PHINode *PN = dyn_cast<PHINode> (*UI)){
					DBG(dbgs() << "\t\tUsed at PHI:" << *PN << "\n");
					if(!BlocksToExtract.count(PN->getParent())){
						// if used in PHI, then substitute the PHI with loads
						DBG(dbgs() << "\t\tDemote PHI\n");
						DemotePHIToStack(PN, 0);
						checkAgain = true;
						break;
					}
				}
				else if (Instruction *I = dyn_cast<Instruction> (*UI)){
					DBG(dbgs() << "\t\tUsed at I:" << *I << "\n");
					if(!BlocksToExtract.count(I->getParent())){
						// if used outside the extracted area, then demote it to stack
						DBG(dbgs() << "\t\tDemote Output to Stack\n");
					    DemoteRegToStack(*dyn_cast<Instruction> (outputs[i]), 0, 0);
					    checkAgain = true;
						break;
					}
				}
			}
		}
	} while (checkAgain);

	DBG(dumb(oldFunction, "(4) After I/O: "));

	/*__________________________________________________________________________________
	 *
	 * 		(5) Create the new function
	 * __________________________________________________________________________________
	 */
	// Construct new function based on inputs/outputs & add allocas for all defs.
	Function *newFunction = constructFunction(
			context,
			module,
			header,
			inputs,
			"lerna_" + oldFunction->getName() + "_" + header->getName(),
			oldFunction->doesNotThrow(),
			false
			);

	Function *newFunctionSeq = constructFunction(
			context,
			module,
			header,
			inputs,
			"lerna_seq_" + oldFunction->getName() + "_" + header->getName(),
			oldFunction->doesNotThrow(),
			true
			);

	DBG(dumb(oldFunction, "(5) New Function Added: "));

	/*__________________________________________________________________________________
	 *
	 * 		(6) Call the function at dispatcher, and emit switch at syncExit
	 * __________________________________________________________________________________
	 */
	CallInst *callDispatch;
	SmallVector<BasicBlock*, 8> exits;
	// Sets up the caller side by adding the call instruction, splitting any PHI nodes in the header block as necessary.
	{
		// Emit a call to the new function, passing in: *pointer to struct (if
		// aggregating parameters), or plan inputs and allocated memory for outputs
		std::vector<Value*> StructValues, ReloadOutputs, Reloads;

		// Add inputs as params, or to be filled into the struct
		for (unsigned i = 0, e = inputs.size(); i != e; ++i)
			StructValues.push_back(inputs[i]);

		Value *Struct = 0;
		if (inputs.size() > 0) {
			std::vector<Type*> ArgTypes;
			for (Values::iterator v = StructValues.begin(), ve = StructValues.end(); v != ve; ++v) {
				ArgTypes.push_back((*v)->getType());
			}
			// Allocate a struct at the beginning of this function
			Type *StructArgTy = StructType::get(context, ArgTypes);
			/*
			 * Allocate Struct on Stack
			 * Cause StackOverFlow as releasing memory at function return
			 *
			 *
					Struct = new AllocaInst(StructArgTy, 0, "structArg", dispatcher);

			*/

			/*
			 * Allocate Struct on Heap using malloc
			 * Free must occur at LernaRuntime not here
			 */
#ifdef _32_BIT
			ConstantInt *val_mem = ConstantInt::get(context, APInt(32, 1));
			Type* IntPtrTy = IntegerType::getInt32Ty(context);
#else
			ConstantInt *val_mem = ConstantInt::get(context, APInt(64, 1));
			const Type* IntPtrTy = IntegerType::getInt64Ty(context);
#endif
			Constant* allocsize = ConstantExpr::getSizeOf(StructArgTy);
			allocsize = ConstantExpr::getTruncOrBitCast(allocsize, IntPtrTy);
			Struct = CallInst::CreateMalloc(dispatcher, IntPtrTy, StructArgTy, allocsize, val_mem, NULL, "arr");
			dispatcher->getInstList().push_back(cast<Instruction>(Struct));

			for (unsigned i = 0, e = inputs.size(); i != e; ++i) {
				Value* arr[] = {
						Constant::getNullValue(Type::getInt32Ty(context)),
						ConstantInt::get(Type::getInt32Ty(context), i)
					};
				ArrayRef<Value *>Idx(arr);
				GetElementPtrInst *GEP = GetElementPtrInst::Create(Struct, Idx, "gep_" + StructValues[i]->getName());
				dispatcher->getInstList().push_back(GEP);
				StoreInst *SI = new StoreInst(StructValues[i], GEP);
				dispatcher->getInstList().push_back(SI);
			}
		}
		//----------------
		// Create a void pointer that points to the aggregated arguments struct
		Type * TyStruct = PointerType::getUnqual(IntegerType::getInt8Ty(context));
		Instruction::CastOps OpCastStruct = CastInst::getCastOpcode(Struct, false, TyStruct, false);
		// Cast the struct to void*
		Instruction *ICastStruct = CastInst::Create(OpCastStruct, Struct,	TyStruct, Struct->getName(), dispatcher);

		// Create a pointer to function that points to the extracted function
		Instruction *ICastFunc;
		{
			Type * arr[] = {
					(Type *) PointerType::getUnqual(IntegerType::getInt8Ty(context)),
			};
			ArrayRef<Type *> TyFuncArgs(arr);
			Type * TyFunc = (Type *) PointerType::getUnqual(FunctionType::get(Type::getVoidTy(context), TyFuncArgs, false));
			Instruction::CastOps OpCastFunc = CastInst::getCastOpcode(newFunction, false,	TyFunc, false);
			// Cast the function to pointer to function
			ICastFunc = CastInst::Create(OpCastFunc, newFunction, TyFunc, newFunction->getName(), dispatcher);
		}
		Instruction *ICastFuncSeq;
		{
			Type * arr[] = {
					(Type *) PointerType::getUnqual(IntegerType::getInt8Ty(context)),
			};
			ArrayRef<Type *> TyFuncArgs(arr);
			Type * TyFunc = (Type *) PointerType::getUnqual(FunctionType::get(Type::getVoidTy(context), TyFuncArgs, false));
			Instruction::CastOps OpCastFunc = CastInst::getCastOpcode(newFunctionSeq, false,	TyFunc, false);
			// Cast the function to pointer to function
			ICastFuncSeq = CastInst::Create(OpCastFunc, newFunctionSeq, TyFunc, newFunctionSeq->getName(), dispatcher);
		}

		// Call the Lerna function
		Value* arr[] = {
				ICastStruct,
				ICastFunc,
				ICastFuncSeq,
				callInit};
		ArrayRef<Value*> ParamFuncLerna(arr);
		callDispatch = CallInst::Create(FuncLernaAddJob,	ParamFuncLerna, "", dispatcher);

		// Handle overflow
		Constant *overflow = ConstantInt::get(IntegerType::getInt8Ty(context), 1);
		ICmpInst *cmpOverflow = new ICmpInst(*dispatcher, CmpInst::ICMP_EQ, callDispatch,	overflow, "flagOverflowCond");
		BranchInst::Create(syncExit, newHeader, cmpOverflow, dispatcher);

		// Now we can emit a switch statement using the call as a value.
		SwitchInst *TheSwitch = NULL;
		// Since there may be multiple exits from the original region, make the new
		// function return an unsigned, switch on that number.  This loop iterates
		// over all of the blocks in the extracted region, updating any terminator
		// instructions in the to-be-extracted region that branch to blocks that are
		// not in the region to be extracted.
		unsigned switchVal = 100;
		for (SetVector<BasicBlock*>::const_iterator	ei = BlocksToExtract.begin(), ee = BlocksToExtract.end(); ei != ee; ++ei) {
			BasicBlock *exit = *ei;
			DBG(dbgs() << "Check Successor of " << exit->getName() << "\n");
			TerminatorInst *TI = exit->getTerminator();
			for (unsigned i = 0, e = TI->getNumSuccessors(); i != e; ++i) {
				DBG(dbgs() << "\t:" << TI->getSuccessor(i)->getName() << "\n");
				if (!BlocksToExtract.count(TI->getSuccessor(i))) {
					DBG(dbgs() << "\t\tOut of extracted region\n");
					BasicBlock *OldTarget = TI->getSuccessor(i);
					// add a new basic block which returns the appropriate value
					BasicBlock *NewTarget = BasicBlock::Create(context, OldTarget->getName() + ".exitStub", newFunction);
					exits.push_back(NewTarget);
					// generate return value
					unsigned SuccNum = OldTarget == newBatch ? DEFAULT_EXIT : switchVal++;

					Value *brVal = ConstantInt::get(Type::getInt32Ty(context), SuccNum);
					ReturnInst::Create(context, brVal, NewTarget);

					// Update the switch instruction.
					if(TheSwitch == NULL){
						TheSwitch = SwitchInst::Create(
										Constant::getNullValue(Type::getInt32Ty(context)),
										OldTarget,
										0, syncExit);
					}else
						TheSwitch->addCase(ConstantInt::get(Type::getInt32Ty(context), SuccNum), OldTarget);

					for (BasicBlock::iterator I = OldTarget->begin(); isa<PHINode> (I); ++I) {
						PHINode *PN = cast<PHINode> (I);
						if(PN)
							for (unsigned i = 0; i != PN->getNumIncomingValues(); ++i)
								if (PN->getIncomingBlock(i) == exit)
									PN->setIncomingBlock(i, syncExit);
					}

					// rewrite the original branch instruction with this new target
					TI->setSuccessor(i, NewTarget);
				}
			}
		}

		// call Sync to wait for invoked jobs (threads)
		Value* arr2 = {
				callInit
		};
		ArrayRef<Value*> ParamFuncLernaSync(arr2);
		CallInst *callSync = CallInst::Create(FuncLernaEndBatch, ParamFuncLernaSync, "");
		syncExit->getInstList().push_front(callSync);
		// decide the exit based on the Sync result
		TheSwitch->setOperand(0, callSync);
	}

	DBG(dumb(oldFunction, "(6) Switch & Call Emitted: "));
	/*__________________________________________________________________________________
	 *
	 * 		(7) Move the code to the new function
	 * __________________________________________________________________________________
	 */
	{
		Function *oldFunc = (*BlocksToExtract.begin())->getParent();
		Function::BasicBlockListType &oldBlocks = oldFunc->getBasicBlockList();
		Function::BasicBlockListType &newBlocks = newFunction->getBasicBlockList();

		// move code to the new function
		for (SetVector<BasicBlock*>::const_iterator	i = BlocksToExtract.begin(), e = BlocksToExtract.end(); i != e; ++i) {
			// Delete the basic block from the old function, and the list of blocks
			oldBlocks.remove(*i);
			// Insert this basic block into the new function
			newBlocks.push_back(*i);
		}

		// clone the code to sequential version
		ValueToValueMapTy VMap;
		Function::arg_iterator DestI = newFunctionSeq->arg_begin();
		for (Function::const_arg_iterator I = newFunction->arg_begin(), E = newFunction->arg_end(); I != E; ++I)
		    if (VMap.count(I) == 0) {   // Is this argument preserved?
		      DestI->setName(I->getName()); // Copy the name over...
		      VMap[I] = DestI++;        // Add mapping to VMap
		    }
		SmallVector<ReturnInst*, 8> returns;
		CloneFunctionInto(newFunctionSeq, newFunction, VMap, false, returns);

#ifdef USE_TANGER
		// -------------------  TX demarcation  -----------------------
		// Tanger Begin
		std::vector<Value*> ParamFuncTanger;
		CallInst::Create(FuncTangerBegin, ParamFuncTanger.begin(),	ParamFuncTanger.end(), "", newFunction->getBasicBlockList().front().getTerminator());

		// Tanger commit
		for (unsigned int i=0; i<exits.size(); i++) {
			std::vector<Value*> ParamFuncTanger;
			CallInst::Create(FuncTangerCommit,	ParamFuncTanger.begin(), ParamFuncTanger.end(),	"", exits[i]->getFirstNonPHI());
		}
#endif
	}
	DBG(dumb(newFunction, "(7) Final: ")); // print the new function
	DBG(dumb(newFunctionSeq, "(7) Final: ")); // print the new function
	DBG(dumb(oldFunction, "(7) Final: ")); // print the original function after transformation
	return 0;
}

// constructFunction - make a function based on inputs
Function *LernaBuilder::constructFunction(
		LLVMContext& context,
		Module *module,
		BasicBlock* header,
		const Values &inputs,
		Twine name,
		bool throwing,
		bool definitionOnly) {

	// This function returns unsigned, outputs will go back by reference.
	Type* RetTy = Type::getInt32Ty(context);

	std::vector<Type*> paramTy;
	// Add the types of the input values to the function's argument list
	for (Values::const_iterator i = inputs.begin(), e = inputs.end(); i != e; ++i) {
		const Value *value = *i;
		paramTy.push_back(value->getType());
	}

/*
	DBG(dbgs() << "Function type: " << *RetTy << " f(");
	for (std::vector<const Type*>::iterator i = paramTy.begin(), e = paramTy.end(); i != e; ++i)
		DBG(dbgs() << **i << ", ");
	DBG(dbgs() << ")\n");
*/

	if (inputs.size() > 0) {
		PointerType *StructPtr = PointerType::getUnqual(StructType::get(
				context, paramTy));
		paramTy.clear();
		paramTy.push_back(StructPtr);
	}
	FunctionType *funcType = FunctionType::get(RetTy, paramTy, false);

	// Create the new function
	Function *newFunction = Function::Create(funcType, GlobalValue::InternalLinkage, name, module);

	// If the old function is no-throw, so is the new one.
	if (throwing)
		newFunction->setDoesNotThrow();

	if(!definitionOnly){
		BasicBlock *newFuncRoot = BasicBlock::Create(context, "newFuncRoot");
		newFuncRoot->getInstList().push_back(BranchInst::Create(header));
		newFunction->getBasicBlockList().push_front(newFuncRoot);

		Value* AI = newFunction->arg_begin();

		// Rewrite all users of the inputs in the extracted region to use the
		// arguments (or appropriate addressing into struct) instead.
		for (unsigned i = 0, e = inputs.size(); i != e; ++i) {
			Value* arr[] = {
					Constant::getNullValue(Type::getInt32Ty(context)),
					ConstantInt::get(Type::getInt32Ty(context), i)
				};
			ArrayRef<Value *>Idx(arr);
			TerminatorInst *TI = newFunction->begin()->getTerminator();
			GetElementPtrInst *GEP = GetElementPtrInst::Create(AI, Idx, "gep_" + inputs[i]->getName(), TI);
			Value *RewriteVal = new LoadInst(GEP, "loadgep_" + inputs[i]->getName(), TI);
			std::vector<User*> Users(inputs[i]->user_begin(), inputs[i]->user_end());	// have to take it aside into a vector, as we change it inside the loop
			for (std::vector<User*>::iterator use = Users.begin(), useE = Users.end(); use != useE; ++use)
				if (Instruction* inst = dyn_cast<Instruction>(*use))
					if (BlocksToExtract.count(inst->getParent()))
						inst->replaceUsesOfWith(inputs[i], RewriteVal);
			if(inputs[i] == newIndVar){
				addMetaData(dyn_cast<LoadInst>(RewriteVal), "indvar");
				for (Value::user_iterator use = RewriteVal->user_begin(), useE = RewriteVal->user_end(); use != useE; ++use){
					dbgs() << "\tUses IndVar @ newFunc: " << *(*use) << "\n";
					if(dyn_cast<StoreInst>(*use) || dyn_cast<LoadInst>(*use)){
						addMetaData(dyn_cast<Instruction>(*use), "indvar");
					}
				}
			}else
				addMetaData(dyn_cast<LoadInst>(RewriteVal), "parameter");
		}
	}

	return newFunction;
}
