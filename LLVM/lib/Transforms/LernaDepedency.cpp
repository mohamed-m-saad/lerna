
#define DEBUG_TYPE "lerna_dependency"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Pass.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Lerna/Lerna.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/ValueTracking.h"
#include "llvm/Analysis/MemoryDependenceAnalysis.h"
#include <iostream>
#include <fstream>
using namespace std;
using namespace llvm;
using namespace lerna;

STATISTIC(LernaDependencyCounter, "Reduce transactional load/store calls within transaction");

//#define DBG(X) X

namespace lerna{
  // LernaDependency - The first implementation, without getAnalysisUsage.
  class LernaDependency : public FunctionPass, LernaPass {
   public:
    static char ID; // Pass identification, replacement for typeid
    LernaDependency() : FunctionPass(ID) {}

    virtual void getAnalysisUsage(AnalysisUsage &AU) const {
	    AU.addRequired<DataLayoutPass> ();
    	AU.addRequiredTransitive<MemoryDependenceAnalysis>();
        AU.addRequiredTransitive<AliasAnalysis>();
        AU.addPreserved<AliasAnalysis>();
  	}

    /**
     * Returns the type that the provided type must be casted to before a
     * load or store.
     */
    const Type* getLoadStoreConversion(Module* M, const DataLayout* TD, Type* type)
    {
        const Type* to = type;
        if (isa<PointerType> (type)) {
            // convert to integer
            if (TD->getPointerSizeInBits() == 32)
                to = IntegerType::get(M->getContext(), 32);
            else if (TD->getPointerSizeInBits() == 64)
                to = IntegerType::get(M->getContext(), 64);
            else assert(0 && "unsupported pointer size");
        }
        else if (type->isFloatingPointTy()) {
            // convert to integer
            if (type == Type::getFloatTy(M->getContext()))
                to = IntegerType::get(M->getContext(), 32);
            else if (type == Type::getDoubleTy(M->getContext()))
                to = IntegerType::get(M->getContext(), 64);
        }
        else if (type->isIntegerTy() && cast<IntegerType> (type)->getBitWidth() < 8) {
            // use a bigger integer type if we are allowed to store more
            // TODO: use this for bigger integers too?
            to = IntegerType::get(M->getContext(), TD->getTypeStoreSizeInBits(type));
        }
        return to;
    }

    virtual bool runOnFunction(Function &F) {
    	if(!F.getName().startswith("lerna_") || F.getName().startswith("lerna_seq")){
    		 return false;
    	}
    	LOG(dbgs() << "Dependency: Processing Function '" << F.getName() << "'\n");

    	Module* M = F.getParent();
    	DataLayoutPass* D = &getAnalysis<DataLayoutPass> ();
    	const DataLayout* DL = &D->getDataLayout();
		// -------------------  Reduce Dependencies  ------------------
    	unsigned int safe_loads = 0, marked_loads = 0, unsafe_loads = 0;
    	unsigned int safe_stores = 0, marked_stores = 0, unsafe_stores = 0;
    	AliasAnalysis &AA = getAnalysis<AliasAnalysis>();
		for(Function::iterator i=F.begin(); i!=F.end(); ++i){
			DBG(dbgs() << "   BB " << i->getName() << "\n");
			for(BasicBlock::iterator j=i->begin();j!=i->end();++j)
			{
				if(StoreInst *SI = dyn_cast<StoreInst>(j)){
					if(SI->getMetadata("lerna")){
						marked_stores++;
						continue;
					}
					const Value *si_v = SI->getPointerOperand();
					if(dyn_cast<AllocaInst>(GetUnderlyingObject(si_v, DL))){
						DBG(dbgs() << "\t\tSafe Store\n");
						addMetaData(SI, "safe");
						safe_stores++;
					}else{
						unsafe_stores++;
					}
				}else if(LoadInst *LI = dyn_cast<LoadInst>(j)){
					if(LI->getMetadata("lerna")){
						marked_loads++;
						continue;
					}
					DBG(dbgs() << "\tLoad: " << *j << "\n");
					const Value *li_v = LI->getPointerOperand();
					uint64_t li_size = AA.UnknownSize;
					const Type* li_type = getLoadStoreConversion(M, DL, li_v->getType());
					if(li_type->isIntegerTy())
						li_size = cast<IntegerType>(li_type)->getBitWidth();
					DBG(dbgs()<<"\t\tSize=" << li_size << "\n");
					DBG(dbgs()<<"\t\tObj=" << *GetUnderlyingObject(li_v, DL) << "\n");
					bool depdendent = false;
					if(dyn_cast<AllocaInst>(GetUnderlyingObject(li_v, DL))){
						DBG(dbgs()<<"\t\tLocal\n");
					}else{
						for(Function::iterator ii=F.begin(); ii!=F.end(); ++ii){
							for(BasicBlock::iterator jj=ii->begin();jj!=ii->end();++jj){
								if(StoreInst *SI = dyn_cast<StoreInst>(jj)){
									DBG(dbgs() << "\t\tStore: " << *SI << "\n");
									const Value *si_v = SI->getPointerOperand();
									uint64_t si_size = AA.UnknownSize;
									const Type* si_type = getLoadStoreConversion(M, DL, si_v->getType());
									if(si_type->isIntegerTy())
										si_size = cast<IntegerType>(si_type)->getBitWidth();
									DBG(dbgs()<<"\t\t\tSize=" << si_size << "\n");
									DBG(dbgs()<<"\t\t\tObj=" << *(GetUnderlyingObject(si_v, DL)) << "\n");
									if(dyn_cast<AllocaInst>(GetUnderlyingObject(si_v, DL))){
										DBG(dbgs()<<"\t\t\tLocal\n");
									}
									if(GetUnderlyingObject(si_v, DL)->getName() == GetUnderlyingObject(li_v, DL)->getName()){
										DBG(dbgs()<<"\t\t\tSame Object!\n");
										depdendent = true; break;
									}
									AliasAnalysis::AliasResult ar = AA.alias(si_v, si_size, li_v, li_size);
									switch(ar){
										case 0: DBG(dbgs()<<"\t\t\tNoAlias\n"); break; 	// No dependencies.
										case 1: DBG(dbgs()<<"\t\t\tMayAlias\n"); break;   	// Anything goes
										case 2: DBG(dbgs()<<"\t\t\tPartialAlias\n"); depdendent = true; break;// Pointers differ, but pointees overlap.
										case 3: DBG(dbgs()<<"\t\t\tMustAlias\n"); depdendent = true; break; 	// Dependent
										default: DBG(dbgs()<<"\t\t\tAlias=" << ar << "\n"); break; 	// Dependent
									}
								}
							}
						}
					}
					if(!depdendent){
						DBG(dbgs() << "\t\tSafe Load\n");
						addMetaData(LI, "safe");
						safe_loads++;
					}else{
						unsafe_loads++;
					}
				}else if(CallInst *CI = dyn_cast<CallInst>(j)){
					DBG(Function* func = CI->getCalledFunction());
					DBG(dbgs() << "\tCall: " << *CI << "\n");
//					DBG(dbgs() << "\t\t" << func->getName() << "\n");
				}
			}
		}
		LOG(dbgs() <<  " Alias Dependency @ " << F.getName() << " : " <<
				"[Safe=" << safe_loads << "/Marked=" << marked_loads << "/Unsafe=" << unsafe_loads << "] Loads, " <<
				"[Safe=" << safe_stores << "/Marked=" << marked_stores << "/Unsafe=" << unsafe_stores << "] Stores, " <<
				"\n");

		MemoryDependenceAnalysis &MDA = getAnalysis<MemoryDependenceAnalysis>();
		for(Function::iterator i=F.begin();i!=F.end();++i){
			DBG(dbgs() << "   BB " << i->getName() << "\n");
			for(BasicBlock::iterator j=i->begin();j!=i->end();++j)
			{
				if(dyn_cast<LoadInst>(j) || dyn_cast<StoreInst>(j)){
					DBG(dbgs() << "\tMem: " << *j << "\n");
					Instruction* I = &(*j);
					DBG(dbgs() << "\t\tDependency:\n");
					do{
						MemDepResult mda_result = MDA.getDependency(I);
						if(!mda_result.getInst() || mda_result.getInst()==I){
							DBG(dbgs() << "\t\t\tOk\n");
							break;
						}
						I = mda_result.getInst();
						DBG(dbgs() << "\t\t\t" <<
								I->getParent()->getName() << " " <<
								mda_result.isClobber() << " " <<
								mda_result.isDef() << " " <<
								mda_result.isNonLocal() << " " <<
								":" << *I << "\n");
					}while(true);

//					Instruction* I2 = &(*j);
					AliasAnalysis::Location Loc;
					if(StoreInst *SI = dyn_cast<StoreInst>(j))
						Loc = AA.getLocation(SI);
					else if(LoadInst *LI = dyn_cast<LoadInst>(j)){
						Loc = AA.getLocation(LI);
					}
					if(dyn_cast<PointerType>(Loc.Ptr->getType())){
						DBG(dbgs() << "\t\tPointer:\n");
						for(Function::iterator i=F.begin();i!=F.end();++i){
							SmallVector<NonLocalDepResult, 8> res;
							MDA.getNonLocalPointerDependency(Loc, false, &(*i), res);
							for(unsigned int j=0; j<res.size(); j++){
								NonLocalDepResult r = res[j];
								MemDepResult mda_result = r.getResult();
								DBG(dbgs() << "\t\t\t" <<
										mda_result.isClobber() << " " <<
										mda_result.isDef() << " " <<
										mda_result.isNonLocal() << " ");
								if(mda_result.getInst()!=NULL){
									DBG(dbgs() << mda_result.getInst()->getParent()->getName().data() << ":" << *mda_result.getInst() << "\n");
								}else{
									DBG(dbgs() << "NULL\n");
								}
							}
						}
					}else{
						DBG(dbgs() << "\t\t\tNon Pointer\n");
					}
				}else if(CallInst *CI = dyn_cast<CallInst>(j)){
					Function* func = CI->getCalledFunction();
					DBG(dbgs() << "\tCall: " << *CI << "\n");
//					DBG(dbgs() << "\t\t" << func->getName() << "\n");
				}
			}
		}
		DBG(dbgs() <<  " Memory Dependency @ " << F.getName().data() << "\n");
		return true;
    }

   private:
   	ofstream dependency;
  };
}

char LernaDependency::ID = 0;
static RegisterPass<LernaDependency>
Y("dependency", "LernaVM: Reduce transactional load/store calls within transaction");

char &lerna::LernaDependencyID = LernaDependency::ID;
FunctionPass *createLernaDependencyPass() { return new LernaDependency(); }
