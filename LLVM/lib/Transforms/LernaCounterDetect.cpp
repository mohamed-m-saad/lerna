/*
 * CounterDetect.cpp
 *
 *       
 */

#define DEBUG_TYPE "lerna_counterDetect"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Pass.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Lerna/Lerna.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/MemoryDependenceAnalysis.h"
#include "llvm/Support/CommandLine.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
using namespace llvm;
using namespace lerna;

STATISTIC(NumDetected, "Number of detected counters");

static cl::opt<std::string>
CheckLoop("check-loop",
					cl::init("-"),
					cl::value_desc("<header basic block>@<function name>"),
                    cl::desc("Only check given loop"));

namespace lerna{
typedef SetVector<Value*> Values;

class LernaCounterDetect: public LernaLoopPass {
public:
	static char ID;

	explicit LernaCounterDetect() :
		LernaLoopPass(ID) {
	}

	virtual bool runOnLoop(Loop *L, LPPassManager &LPM);

	virtual void getAnalysisUsage(AnalysisUsage &AU) const {
		AU.addRequiredTransitive<MemoryDependenceAnalysis>();
		AU.addRequiredTransitive<AliasAnalysis>();
		AU.addPreserved<AliasAnalysis>();
	}

private:
	int detectCounters(SmallVector<BasicBlock*, 8> code);
};

}

char LernaCounterDetect::ID = 0;

static RegisterPass<LernaCounterDetect>
Y("counterDetect", "LernaVM: Detect counters that can be replaced with atomic load&increment");

char &lerna::LernaCounterDetectID = LernaCounterDetect::ID;
LoopPass *createLernaCounterDetectPass() {
	return new LernaCounterDetect();
}

int LernaCounterDetect::detectCounters(SmallVector<BasicBlock*, 8> code){
	LLVMContext& Context = code[0]->getContext();
	Module* module = code[0]->getParent()->getParent();
	Type* arr1[] = { (Type *) PointerType::getUnqual(IntegerType::getInt32Ty(Context)), (Type *) IntegerType::getInt32Ty(Context)	};
	ArrayRef<Type *> TyFuncLernaIntIncrementArgs(arr1);
	Constant* FuncLernaIntIncrement = module->getOrInsertFunction(HYDRARUNTIME_INC_TX_32bit, FunctionType::get(Type::getVoidTy(Context), TyFuncLernaIntIncrementArgs, false));

	Type* arr2[] = {(Type *) PointerType::getUnqual(IntegerType::getFloatTy(Context)), (Type *) IntegerType::getFloatTy(Context) };
	ArrayRef<Type *> TyFuncLernaFPIncrementFPArgs(arr2);
	Constant* FuncLernaFPIncrementFP = module->getOrInsertFunction(HYDRARUNTIME_INC_TX_FLOAT_FLOAT, FunctionType::get(Type::getVoidTy(Context), TyFuncLernaFPIncrementFPArgs, false));

	Type* arr3[] = {(Type *) PointerType::getUnqual(IntegerType::getFloatTy(Context)), (Type *) IntegerType::getDoubleTy(Context) };
	ArrayRef<Type *> TyFuncLernaFPIncrementDoubleArgs(arr3);
	Constant* FuncLernaFPIncrementDouble = module->getOrInsertFunction(HYDRARUNTIME_INC_TX_FLOAT_DOUBLE, FunctionType::get(Type::getVoidTy(Context), TyFuncLernaFPIncrementDoubleArgs, false));

	Type* arr4[] = { (Type *) PointerType::getUnqual(IntegerType::getDoubleTy(Context)), (Type *) IntegerType::getDoubleTy(Context) };
	ArrayRef<Type *> TyFuncLernaDoubleIncrementArgs(arr4);
	Constant* FuncLernaDoubleIncrement = module->getOrInsertFunction(HYDRARUNTIME_INC_TX_DOUBLE, FunctionType::get(Type::getVoidTy(Context), TyFuncLernaDoubleIncrementArgs, false));

	DBG(dumb(code[0]->getParent(), "Before"));

	DBG(dbgs() << "\Suspected Counters on " << code.size() << " Basic Blocks\n");

	// get all loads
	SmallVector<LoadInst*, 8> Loads;
	for (unsigned x = 0; x != code.size(); ++x) {
		BasicBlock* i = code[x];
		for(BasicBlock::iterator j=i->begin();j!=i->end();++j) {
			if(LoadInst *LI = dyn_cast<LoadInst>(j)){
				Loads.push_back(LI);
			}
		}
	}

	SmallVector<LoadInst*, 8> erasedLoads;
	unsigned int counters = 0;
	AliasAnalysis &AA = getAnalysis<AliasAnalysis>();
	for (unsigned x = 0; x != Loads.size(); ++x) {
		LoadInst *LI = Loads[x];
		const Value *li_v = LI->getPointerOperand();
		DBG(dbgs() << "\t\t\tLoad [" << *li_v <<  "] " << *LI << "\n");
		bool loadMoreThanOnce = false;
		// check if no other loads uses the same address
		for (unsigned y = 0; y != Loads.size() && !loadMoreThanOnce; ++y) {
				LoadInst *LI2 = Loads[y];
				if(LI == LI2) continue;
				const Value *li2_v = LI2->getPointerOperand();
				AliasAnalysis::AliasResult ar = AA.alias(li_v, li2_v);
				if(ar){
					DBG(dbgs() << "\t\t\t\tLoaded Again (alias=" << ar << ") at : " << *LI2 << "\n");
					loadMoreThanOnce = true;
					break;
				}else{
					DBG(dbgs() << "\t\t\t\tSkip (alias=" << ar << ") at [" << *li2_v <<  "] :" << *LI2 << "\n");
				}
		}
		if(loadMoreThanOnce)
			continue;
		bool counterDetected = false;
		// search for store to the same address
		// TODO handle case of multiple redundant stores to same address
		for (unsigned y = 0; y != code.size() && !counterDetected; ++y) {
			BasicBlock* ii = code[y];
			for(BasicBlock::iterator jj=ii->begin();jj!=ii->end();++jj){
				if(StoreInst *SI = dyn_cast<StoreInst>(jj)){
					DBG(dbgs() << "\t\t\t\tStore: " << *SI << "\n");
					const Value *si_v = SI->getPointerOperand();
					AliasAnalysis::AliasResult ar = AA.alias(si_v, li_v);
					if(ar){
						// check the store operand
						Value* si_v2 = SI->getValueOperand();
						if(Instruction *Inst = dyn_cast<Instruction>(si_v2)){
							SmallVector<Instruction*, 5> erase;
							erase.push_back(SI);

							// check if the stored value is   constant + loaded_value
							if(Inst->getOpcode() == Instruction::FPTrunc){
								erase.push_back(Inst);
								// sometimes floating point operation truncate double/float
								Inst = dyn_cast<Instruction>(Inst->getOperand(0)); // point to the original operation
							}
							if ((	Inst->getOpcode() == Instruction::Add ||
									Inst->getOpcode() == Instruction::FAdd
								) && isa<Constant>(Inst->getOperand(1))){
								erase.push_back(Inst);
								Value* vv = Inst->getOperand(0);
								if(FPExtInst *fbex = dyn_cast<FPExtInst>(vv)){
									erase.push_back(fbex);
									// sometimes floating point operation extract double/float
									vv = fbex->getOperand(0); // point to the original alloca
								}
								AliasAnalysis::AliasResult a2 = AA.alias(LI, vv);
								DBG(dbgs() << "\t\t\t\t\tOperand: " << *si_v2 << "\t\t" << a2 << "\n");
								if(a2){
									Value* arr[] = { SI->getPointerOperand(), Inst->getOperand(1) } ;
									ArrayRef<Value*> ParamFuncLerna( arr );
									DBG(dbgs() << "\t\t\t\t\tType: " << *SI->getPointerOperand()->getType() << " + " << *Inst->getOperand(1)->getType() << "\n");
									Constant* increment_func =
											Inst->getOpcode() == Instruction::Add ?
													FuncLernaIntIncrement :	// integer counter
													SI->getPointerOperand()->getType() == IntegerType::getDoublePtrTy(Context) ?
														FuncLernaDoubleIncrement :	// double counter
														Inst->getOperand(1)->getType() == IntegerType::getDoubleTy(Context) ?
																FuncLernaFPIncrementDouble :	// float counter with double increment
																FuncLernaFPIncrementFP;			// float counter with float increment
									DBG(dbgs() << "\t\t\t\t\tCalling: " << *increment_func << "\n");
									CallInst::Create(increment_func, ParamFuncLerna, "", SI);
									// remove load-modify-store
									for (unsigned r = 0; r != erase.size(); ++r) {
										erase[r]->eraseFromParent();
									}
									erasedLoads.push_back(LI);
									// found counter
									counterDetected =  true;
									break;
								}
							}
						}
					}
				}
			}
		}
		if(counterDetected){
			DBG(dbgs() << "\t\t\t\tCounter Found !!!\n");
			counters++;
		}
	}

	for (unsigned r = 0; r != erasedLoads.size(); ++r) {
		erasedLoads[r]->eraseFromParent();
	}

	DBG(dumb(code[0]->getParent(), "After"));

	return counters;
}

bool LernaCounterDetect::runOnLoop(Loop *L, LPPassManager &LPM) {
	LOG(dbgs() << "CounterDetect: Processing Loop '" << L->getBlocks().front()->getName() << "' at " << L->getBlocks().front()->getParent()->getName() << "() : Blocks = " << L->getBlocks().size());

	std::string ss = std::string(L->getHeader()->getName()) + "@" + std::string(L->getHeader()->getParent()->getName());
	if(CheckLoop == "-") return false;
	std::stringstream sss(CheckLoop);
	std::string item;
	bool foundInArg = false;
	while (!foundInArg && std::getline(sss, item, ',')) {
		if(ss == item){
			foundInArg = true;
		}
	}
	if(!foundInArg){
		LOG(dbgs() << ": Skip Loop '" << ss << "' [ check-loop='" << CheckLoop << "' ]\n");
		return false;
	}
	SmallVector<BasicBlock*, 8> blocksToExtract;
	for (unsigned i = 0; i != L->getBlocks().size(); ++i) {
		BasicBlock* bb = L->getBlocks()[i];
		blocksToExtract.push_back(bb);
	}

	DBG("\n");
	int counters = detectCounters(blocksToExtract);
	LOG(dbgs() << ": Detected Counters = " << counters << "\n");
	NumDetected += counters;

	return counters > 0;
}

