/*
 * Checker.cpp
 *
 *       
 */

#include "llvm/Support/Debug.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Transforms/Lerna/Lerna.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <sstream>


using namespace std;
using namespace llvm;
using namespace lerna;

void LernaPass::addMetaData(Instruction* i, std::string metadataValue){
	LLVMContext& C = i->getContext();
    SmallVector<Value *, 1> Elts;
    Elts.push_back(MDString::get(C, metadataValue));
    MDNode *Node = MDNode::get(C, Elts);
	i->setMetadata(i->getParent()->getParent()->getParent()->getMDKindID("lerna"), Node);
}

void LernaPass::dumb(Function* function, StringRef comment) {
	Function::BasicBlockListType &code = function->getBasicBlockList();
	LOG(dbgs() << "_______________________________________________________________________________\n");
	LOG(dbgs() << "                                    "  <<  comment << " " << function->getName() <<  "\n");
	LOG(dbgs() << "_______________________________________________________________________________\n");
	for (Function::BasicBlockListType::iterator bs = code.begin(), es = code.end(); bs != es; ++bs) {
		  BasicBlock* bbb = &(*bs);
		  LOG(dbgs() << bbb->getName() << ":\n");
		  for (BasicBlock::iterator I = bbb->begin(), E = bbb->end(); I != E; ++I) {
			  LOG(dbgs() << *I << "\n");
		  }
	}
	LOG(dbgs() << "_______________________________________________________________________________\n");
}

int LernaPass::checkInvalidCalls(SetVector<BasicBlock*> blocks){
	  DBG(dbgs() << "\tChecking: Alloca & External Calls\n");
	  for (SetVector<BasicBlock *>::iterator bs = blocks.begin(), es = blocks.end(); bs != es; bs++) {
    	  BasicBlock* bb = *bs;
	      for (BasicBlock::const_iterator I = bb->begin(), Ie = bb->end();  I != Ie; ++I)
	        if (isa<AllocaInst>(*I)){
	        	return ALLOCA;
	        }
	        else if (const CallInst *CI = dyn_cast<CallInst>(I))
	          if (const Function *F = CI->getCalledFunction()){
	        	if(F->getName().find("lernaruntime_", 0) != StringRef::npos){
	        		continue;
	        	}
	        	if(!lookupDictionary(F, "DICTIONARY.txt") && !lookupDictionary(F, "SAFE.txt")){
	        		LOG(dbgs() << "\t\tExternal Call: \t" << F->getName() << " @ " << bb->getName() << "\n");
	        		return EXTERNAL_CALL;
	        	}
//	        	Disabled at migration to 3.5
//	            if (F->getIntrinsicID() == Intrinsic::vastart){
//					return ALLOCA;
//	            }
	          }
	  }
	  return EXTRACTABLE;
}

std::string LernaPass::getGraphNode(llvm::BasicBlock* bb, bool color){
	unsigned int loads=0, stores=0, safeLoads=0;
	for(BasicBlock::iterator j=bb->begin();j!=bb->end();++j)
		if(dyn_cast<LoadInst>(j)){
			loads++;
			if(j->getMetadata("lerna"))
				safeLoads++;
		}
		else if(dyn_cast<StoreInst>(j)) stores++;
	std::stringstream ss;
	ss << "\"" << bb->getName().str();
	if(loads > 0) ss << " " << safeLoads << "/" << loads << "L";
	if(stores > 0) ss << " " << stores << "S";
	ss << "\"";
	if(color && (loads > 0 || stores>0))
		ss << (loads != safeLoads || stores > 0 ? " [color=red] " : " [color=green] ");
	return ss.str();
}

void LernaPass::printGraph(llvm::SetVector<BasicBlock*> blocks, std::string out_file){
	std::string line;

	ofstream out;
	bool save = out_file.length()>0;
	if(save) out.open(out_file.c_str());

	line = "\tdigraph G {\n";
	if(save) out << line;
	DBG(dbgs() << line);

	for (SetVector<BasicBlock *>::iterator bs = blocks.begin(), es = blocks.end(); bs != es; bs++) {
  		BasicBlock* bb = *bs;
  		line = "\t\t" + getGraphNode(bb, true) + ";\n";
		if(save) out << line;
		DBG(dbgs() << line);
		TerminatorInst *bbTI = bb->getTerminator();
		int successors = bbTI->getNumSuccessors();
		for(int i=0; i<successors; i++){
			line = "\t\t" + getGraphNode(bb) + " -> " + getGraphNode(bbTI->getSuccessor(i)) + ";\n";
			if(save) out << line;
			DBG(dbgs() << line);
		}
	}

	line = "\t}\n";
	if(save) out << line;
	DBG(dbgs() << line);

	if(save) out.close();
}

// Requirements:
//  1- This is a single-entry code region, if it is not we got out of it.
//  2- Find out the header, it is the only block reachable from outside the region.
//  3- Induction Variable block in simple form, i.e. ends with compare instruction with conditional branch
//  4- Deny code region if it contains allocas or vastarts.
int LernaLoopPass::checkExtractableLoop(Loop *L){
	  std::string loopName = L->getBlocks().front()->getName();
	  // If LoopSimplify form is not available, stay out of trouble.
	  DBG(dbgs() << "\tChecking: Complex Loop '" << loopName << "'\n");
	  if (!L->isLoopSimplifyForm()){
		LOG(dbgs() << "\tSkip: Complex Loop '" << loopName << "'\n");
	    return COMPLEX;
	  }

	  DBG(dbgs() << "\tChecking: Refactored Loop '" << loopName << "'\n");
	  bool refactored = L->getHeader()->getParent()->getName().startswith("lerna_");	  // FIXME: nesting not supported so far
	  if(refactored){
		  LOG(dbgs() << "\tSkip: Refactored Loop '" << loopName << "'\n");
		  return REFACTORED;
	  }

	  // Initialize loop data.
	  DBG(dbgs() << "\tChecking: Induction Var '" << loopName << "'\n");
	  PHINode *IndVar = L->getCanonicalInductionVariable();
	  /*
	   * INDVAR SIMPLICATION IS NOT SUPPORTED
	   * ============================================
	   * Starting from LLVM 2.9 they added new option to enable rewrite indvar (-enable-iv-rewrite)
	   * Starting from LLVM 3.0 they removed that from the core as it complicate the design
	   * https://www.bountysource.com/issues/1566708-remove-the-need-for-independent-blocks-and-enable-iv-rewrite
	   * http://lists.cs.uiuc.edu/pipermail/llvmdev/2012-January/046819.html
	   * From now on, we need to maintain the code from old version and use it in our base
	   */
	  if (!IndVar){
		  LOG(dbgs() << "\tSkip: No Induction Var '" << loopName << "'\n");
		  DBG(dbgs() << "\tNo Induction Var '" << loopName << "'\n");
		  return INVALID_INDVAR;
	  }


	  DBG(dbgs() << "\tChecking: IndVar '" << loopName << "'\n");
  	  DBG(dbgs() << "\t\tIndVar block term " << *IndVar->getParent()->getTerminator() << "\n");
	  BranchInst* IndVarBr = dyn_cast<BranchInst>(IndVar->getParent()->getTerminator());
	  if(IndVarBr){
		  if(IndVarBr->getNumSuccessors()==1){
			  DBG(dbgs() << "\tComposite IndVar condition, it doesn't contains conditional branch at end of InvVarBlock '" << IndVar->getParent()->getName() << "'.\n");
			  LOG(dbgs() << "\tSkip: Composite IndVar '" << loopName << "'\n");
			  return INVALID_INDVAR;
		  }
		  ICmpInst* IndVarCond = dyn_cast<ICmpInst>(IndVarBr->getCondition());
		  if(!IndVarCond){
			  DBG(dbgs() << "\tNot Compare IndVar Condition : '" << *IndVarBr->getCondition() << "' ----- '" << *IndVarBr << "'\n");
			  LOG(dbgs() << "\tSkip: Unconditional IndVar '" << loopName << "'\n");
			  return INVALID_INDVAR;
		  }
		  bool usesIndVar = false;
		  for (User::op_iterator O = IndVarCond->op_begin(), E = IndVarCond->op_end(); O != E; ++O){
			  if (*O == IndVar){
				  usesIndVar = true;
				  break;
			  }
		  }
		  if(!usesIndVar){
			  LOG(dbgs() << "\tSkip: IndVar condition doesn't use IndVar '" << loopName << "'\n");
			  return INVALID_INDVAR;
		  }
	  }else{
		  // Invoke can be the the IndVar Block terminator by calling function and return
		  // to given label or throw an exception and go to another label
		  // " The ‘invoke‘ instruction causes control to transfer to a specified function,
		  // with the possibility of control flow transfer to either the ‘normal‘ label or the
		  // ‘exception‘ label. If the callee function returns with the “ret” instruction, control
		  // flow will return to the “normal” label. If the callee (or any indirect callees)
		  // returns via the “resume” instruction or other exception handling mechanism "
		  return INVALID_INDVAR;
	  }

	  DBG(dbgs() << "\tChecking: ShouldExtractLoop '" << loopName << "'\n");
	  // If there is more than one top-level loop in this function, extract all of
	  // the loops. Otherwise there is exactly one top-level loop; in this case if
	  // this function is more than a minimal wrapper around the loop, extract
	  // the loop.
	  bool ShouldExtractLoop = false;
	  {
		  //FIXME need to be revised
		  // Extract the loop if the entry block doesn't branch to the loop header.
		  TerminatorInst *EntryTI =  L->getHeader()->getParent()->getEntryBlock().getTerminator();
		  EntryTI->dump();
		  if (!isa<BranchInst>(EntryTI) || !cast<BranchInst>(EntryTI)->isUnconditional() ||  EntryTI->getSuccessor(0) != L->getHeader())
			  ShouldExtractLoop = true;
		  else {
			// Check to see if any exits from the loop are more than just return
			// blocks.
			SmallVector<BasicBlock*, 8> ExitBlocks;
			L->getExitBlocks(ExitBlocks);
			dbgs() << "Exists = " << ExitBlocks.size() << "\n";
			for (unsigned i = 0, e = ExitBlocks.size(); i != e; ++i){
			  ExitBlocks[i]->getTerminator()->dump();
			  if (!isa<ReturnInst>(ExitBlocks[i]->getTerminator())) {
				ShouldExtractLoop = true;
				break;
			  }
			}
		  }
	  }

	  if(ShouldExtractLoop == false){
		  LOG(dbgs() << "\tSkip: ShouldExtractLoop is false '" << loopName << "'\n");
		  return WRAPPER;
	  }

	  DBG(dbgs() << "\tExtracting region from '" << loopName << "'\n");
	  DBG(dbgs() <<  "\t\tPreheader: " << L->getLoopPreheader()->getName() << "\n");
	  DBG(dbgs() <<  "\t\tHeader: " << L->getHeader()->getName() << "\n");
	  SetVector<BasicBlock*> blocksToExtract;
	  for (unsigned i = 0; i != L->getBlocks().size(); ++i) {
	    	BasicBlock* bb = L->getBlocks()[i];
	    	blocksToExtract.insert(bb);
			DBG(dbgs() << "\t\tAdd block " + bb->getName() + "\n");
	  }
	  int entries = 0;
	  for (SetVector<BasicBlock *>::iterator bs = blocksToExtract.begin(), es = blocksToExtract.end(); bs != es; bs++) {
	  		BasicBlock* bbb = *bs;
	  		for (Value::user_iterator us = bbb->user_begin(), ue = bbb->user_end(); us	!= ue; us++) {
	  			TerminatorInst *TI = dyn_cast<TerminatorInst> (*us);
	  			if (TI && !blocksToExtract.count(TI->getParent())){
	  				DBG(dbgs() << "\t\tEntry: " << *TI << "\n");
	  				entries++;
	  			}
	  		}
	  }
	  if(entries!=1){
		  LOG(dbgs() << "\tSkip: Multiple Entries '" << loopName << "'\n");
		  return NO_SIGNLE_ENTRY;
	  }

	  int check = checkInvalidCalls(blocksToExtract);
	  if(check != EXTRACTABLE){
		  if(check == ALLOCA)
		      LOG(dbgs() << "\tSkip: Found AllocaInst '" << loopName << "'\n");
		  else if(check == EXTERNAL_CALL)
			  LOG(dbgs() << "\tSkip: Calling external function '" << loopName << "'\n");
		  else
			  LOG(dbgs() << "\tSkip: Invalid Call '" << loopName << "'\n");
		  return check;
	  }

	  DBG(dbgs() << "\tChecking: Exit with PHI at start '" << loopName << "'\n");
	  SmallVector<BasicBlock*, 8> ExitBlocks;
	  L->getExitBlocks(ExitBlocks);
	  for (SmallVectorImpl<BasicBlock*>::iterator BI = ExitBlocks.begin(), BE = ExitBlocks.end(); BI != BE; ++BI){
		  for (BasicBlock::iterator I = (*BI)->begin(), E = (*BI)->end(); I != E; ++I) {
		  		  PHINode *PN = dyn_cast<PHINode>(I);
		  		  if (PN && PN->getNumIncomingValues() > 1){
					  LOG(dbgs() << "\tSkip: Exit '" << (*BI)->getName() << "' with PHI '" << *I << "' at start '" << loopName << "'\n");	//FIXME can be handled
					  return START_PHI;
		  		  }
		  }
	  }
	  LOG(dbgs() << "\tEligible.\n");
	  return EXTRACTABLE;
}

bool LernaPass::lookupDictionary(const llvm::Function* f, string dictionary_name){
	string name = f->getName();
	string line;
	ifstream dictionary (dictionary_name.c_str());
	DBG(dbgs() << "\tLookup '" << name << "'\n");
	if (dictionary.is_open())
	{
		DBG(dbgs() << "\tReading '" << dictionary_name << "'\n");
	    while ( getline (dictionary,line) )
	    {
	    	if(name == line)
	    		return true;
	    	DBG(dbgs() << "\t\tLine '" << line << "'\n");
	    }
	    dictionary.close();
	}

	DBG(dbgs() << "\tExternal Function Call '" << name << "' not found at '" << dictionary_name << "'\n");

	ifstream in_dictionary("UNSAFE.txt");
	stringstream ss;
	bool found = false;
	if (in_dictionary.is_open()) {
		string line;
		while (getline(in_dictionary, line)) {
			int t = line.find(" ", 0);
			string func = line.substr(0, t);
			string count = line.substr(t + 1);
			if (name == func) {
				found = true;
				int v = atoi(count.c_str());
				ss << func << " " << (v + 1) << std::endl;
			} else {
				ss << line << std::endl;
			}
		}
		in_dictionary.close();
	}
	if (!found) {
		ss << name << " 1" << std::endl;
	}
	ofstream out_dictionary;
	out_dictionary.open("UNSAFE.txt", ios::out | ios::binary);
	out_dictionary << ss.str();
	out_dictionary.flush();
	out_dictionary.close();

	return false;
}
