/*
 * Lerna.h
 *
 *  Created on: Mar 14, 2013
 *       
 */

#ifndef HYDRA_H_
#define HYDRA_H_

#include "llvm/Pass.h"
#include "llvm/IR/Instruction.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/ADT/SetVector.h"

//------------------------- Name Mangling --------------------------//
#define HYDRARUNTIME_INIT 					"_Z17lernaruntime_initv"
#define HYDRARUNTIME_DESTORY				"_Z20lernaruntime_destoryv"

#define HYDRARUNTIME_NEW_BATCH 				"_Z21lernaruntime_newBatchi"
#define HYDRARUNTIME_ADD_JOB 				"_Z19lernaruntime_addJobPvPFiS_ES1_i"
#define HYDRARUNTIME_END_BATCH 				"_Z21lernaruntime_endBatchi"
#define HYDRARUNTIME_JOBS_COUNT 			"_Z25lernaruntime_getJobsCountv"

#define HYDRARUNTIME_START_TX 				"_Z20lernaruntime_startTxPA1_13__jmp_buf_tagj"
#define HYDRARUNTIME_END_TX 				"_Z18lernaruntime_endTxv"


#define HYDRARUNTIME_READ_TX_Ptr 			"_Z23lernaruntime_readTx_PtrPPvPN3stm8TxThreadE"
#define HYDRARUNTIME_WRITE_TX_Ptr 			"_Z24lernaruntime_writeTx_PtrPPvS_PN3stm8TxThreadE"
#define HYDRARUNTIME_READ_TX_8bit 			"_Z24lernaruntime_readTx_8bitPhPN3stm8TxThreadE"
#define HYDRARUNTIME_WRITE_TX_8bit 			"_Z25lernaruntime_writeTx_8bitPhhPN3stm8TxThreadE"
#define HYDRARUNTIME_READ_TX_16bit 			"_Z25lernaruntime_readTx_16bitPtPN3stm8TxThreadE"
#define HYDRARUNTIME_WRITE_TX_16bit 		"_Z26lernaruntime_writeTx_16bitPttPN3stm8TxThreadE"
#define HYDRARUNTIME_READ_TX_32bit 			"_Z25lernaruntime_readTx_32bitPjPN3stm8TxThreadE"
#define HYDRARUNTIME_WRITE_TX_32bit 		"_Z26lernaruntime_writeTx_32bitPjjPN3stm8TxThreadE"
#define HYDRARUNTIME_INC_TX_32bit 			"_Z24lernaruntime_incTx_32bitPjj"
#define HYDRARUNTIME_READ_TX_64bit 			"_Z25lernaruntime_readTx_64bitPyPN3stm8TxThreadE"
#define HYDRARUNTIME_WRITE_TX_64bit 		"_Z26lernaruntime_writeTx_64bitPyyPN3stm8TxThreadE"
#define HYDRARUNTIME_READ_TX_FLOAT			"_Z25lernaruntime_readTx_floatPfPN3stm8TxThreadE"
#define HYDRARUNTIME_WRITE_TX_FLOAT 		"_Z26lernaruntime_writeTx_floatPffPN3stm8TxThreadE"
#define HYDRARUNTIME_INC_TX_FLOAT_FLOAT		"_Z30lernaruntime_incTx_float_floatPff"
#define HYDRARUNTIME_INC_TX_FLOAT_DOUBLE	"_Z31lernaruntime_incTx_float_doublePfd"
#define HYDRARUNTIME_READ_TX_DOUBLE 		"_Z26lernaruntime_readTx_doublePdPN3stm8TxThreadE"
#define HYDRARUNTIME_WRITE_TX_DOUBLE		"_Z27lernaruntime_writeTx_doublePddPN3stm8TxThreadE"
#define HYDRARUNTIME_INC_TX_DOUBLE			"_Z30lernaruntime_incTx_double_floatPff"

//---------------------------- Logging -----------------------------//
#define LOG(X) X

//#define DBG(X) X
#define DBG(X)

//--------------------------- Constants ----------------------------//
#define EXTRACTABLE	  		0
#define COMPLEX		  		1
#define REFACTORED		 	2
#define INVALID_INDVAR		3
#define WRAPPER				4
#define ALLOCA				5
#define START_PHI			6
#define EXTERNAL_CALL		7
#define NO_SIGNLE_ENTRY		8

namespace lerna {

class LernaPass{
public:

	void dumb(llvm::Function* function, llvm::StringRef comment="");

	void addMetaData(llvm::Instruction* i, std::string metadataValue);

	int checkInvalidCalls(llvm::SetVector<llvm::BasicBlock*> blocks);
	void printGraph(llvm::SetVector<llvm::BasicBlock*> blocks, std::string out_file = "");

	bool lookupDictionary(const llvm::Function* f, std::string dictionary);
private:
	std::string getGraphNode(llvm::BasicBlock* bb, bool color = false);
};

class LernaLoopPass: public llvm::LoopPass, public LernaPass {
public:
	explicit LernaLoopPass(char ID) :
		LoopPass(ID){
	}

	int checkExtractableLoop(llvm::Loop *L);

	const char *getPassName() const {
		return "Lerna";
	}
};


//===----------------------------------------------------------------------===//
//
// LernaDictionary - Build dictionary of functions that are safe to call within transaction.
//
llvm::FunctionPass *createLernaDictionaryPass();
extern char &LernaDictionaryID;

//===----------------------------------------------------------------------===//
//
// LernaDependency - Reduce transactional load/store calls within transaction.
//
llvm::FunctionPass *createLernaDependencyPass();
extern char &LernaDependencyID;

//===----------------------------------------------------------------------===//
//
// LernaIndirection - Transforms usage of any scaler into pointer to the scaler.
//
llvm::FunctionPass *createLernaIndirectionPass();
extern char &LernaIndirectionID;


//===----------------------------------------------------------------------===//
//
// LernaBuilder - Reconstruct loops to run iterations in parallel as functions.
//
llvm::LoopPass *createLernaBuilderPass();
extern char &LernaBuilderID;


//===----------------------------------------------------------------------===//
//
// LernaChecker - Check eligible loops for parallellization.
//
llvm::LoopPass *createLernaCheckerPass();
extern char &LernaCheckerID;


//===----------------------------------------------------------------------===//
//
// LernaCounterDetect - Detect counters that can be replaced with atomic call.
//
llvm::LoopPass *createLernaCounterDetectPass();
extern char &LernaCounterDetectID;


//===----------------------------------------------------------------------===//
//
// LernaChecker - Generate CFG.
//
llvm::FunctionPass *createLernaGrapherPass();
extern char &LernaGrapherID;

//===----------------------------------------------------------------------===//
//
// LernaTransactifier - Transactify Code.
//
llvm::FunctionPass *createLernaTransactifierPass();
extern char &LernaTransactifierID;

//===----------------------------------------------------------------------===//
//
// LernaAnnotation - Use user annotation for transformation enhancements.
//
llvm::FunctionPass *createLernaAnnotationPass();
extern char &LernaAnnotationID;


//===----------------------------------------------------------------------===//
//
// LernaInliner - force inlining.
//
llvm::FunctionPass *createLernaInlinerPass();
extern char &LernaInlinerID;

//===----------------------------------------------------------------------===//
//
// LernaProfiler - Instrument Basic Blocks with print-out for debugging.
//
llvm::BasicBlockPass *createLernaProfilerPass();
extern char &LernaProfilerID;

}

#endif /* HYDRA_H_ */
