# Lerna 1.0 #

Lerna is an end-to-end tool that automatically and transparently detects and extracts parallelism from sequential code using speculation combined with a set of techniques including code profiling, dependency analysis, instrumentation, and adaptive execution.
Speculation is needed because Lerna finds its sweet spot in applications hard-to-parallelize because of data dependency. The parallel execution exploits memory transactions to manage concurrent and out-of-order memory accesses. This scheme allows Lerna to parallelize sequential applications with data dependencies.