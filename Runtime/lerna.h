
#ifndef _HYDRA_H_
# define _HYDRA_H_

#include "stm/txthread.hpp"

void lernaruntime_init();
void lernaruntime_destory();

int lernaruntime_newBatch(int loopId);
int  lernaruntime_addJob(void* args, int (*work_para)(void*), int (*work_seq)(void*), int nested);
int lernaruntime_getJobsCount();
int lernaruntime_endBatch(int nested);

stm::TxThread* lernaruntime_startTx(jmp_buf *_jmpbuf, uint32_t abort_flags);
void lernaruntime_endTx();

void* lernaruntime_readTx_Ptr(void** addr, stm::TxThread* thread);
void lernaruntime_writeTx_Ptr(void** addr, void* val, stm::TxThread* thread);

uint8_t lernaruntime_readTx_8bit(uint8_t* addr, stm::TxThread* thread);
void lernaruntime_writeTx_8bit(uint8_t* addr, uint8_t val, stm::TxThread* thread);

uint16_t lernaruntime_readTx_16bit(uint16_t* addr, stm::TxThread* thread);
void lernaruntime_writeTx_16bit(uint16_t* addr, uint16_t val, stm::TxThread* thread);

uint32_t lernaruntime_readTx_32bit(uint32_t* addr, stm::TxThread* thread);
void lernaruntime_writeTx_32bit(uint32_t* addr, uint32_t val, stm::TxThread* thread);
void lernaruntime_incTx_32bit(uint32_t* addr, uint32_t val);

uint64_t lernaruntime_readTx_64bit(uint64_t* addr, stm::TxThread* thread);
void lernaruntime_writeTx_64bit(uint64_t* addr, uint64_t val, stm::TxThread* thread);

float lernaruntime_readTx_float(float* addr, stm::TxThread* thread);
void lernaruntime_writeTx_float(float* addr, float val, stm::TxThread* thread);
void lernaruntime_incTx_float_double(float* addr, float val);
void lernaruntime_incTx_float_double(float* addr, double val);

double lernaruntime_readTx_double(double* addr, stm::TxThread* thread);
void lernaruntime_writeTx_double(double* addr, double val, stm::TxThread* thread);

#endif
